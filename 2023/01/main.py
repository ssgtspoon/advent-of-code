import re


def part_one():
    with open("input.txt", "r") as input_file:
        total = 0
        for line in input_file:
            line = line.strip()
            digits = "".join([x for x in line if x.isdigit()])
            total += int(f"{digits[0]}{digits[-1]}")

    return total


def part_two() -> int:
    digits_re = re.compile(r"(one|two|three|four|five|six|seven|eight|nine)")
    digit_map = {
        "one": "1",
        "two": "2",
        "three": "3",
        "four": "4",
        "five": "5",
        "six": "6",
        "seven": "7",
        "eight": "8",
        "nine": "9",
    }
    with open("input.txt", "r") as input_file:
        total = 0
        for line in input_file:
            line = line.strip()
            digits = ""
            for i, x in enumerate(line):
                if x.isdigit():
                    digits += x
                elif match := digits_re.match(line[i:]):
                    digits += digit_map[match.group(0)]

            total += int(f"{digits[0]}{digits[-1]}")

    return total


if __name__ == "__main__":
    print(f"Part 1 - {part_one()}")
    print(f"Part 2 - {part_two()}")
