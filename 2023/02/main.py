from dataclasses import dataclass


@dataclass
class Pull:
    red, green, blue = 0, 0, 0

    def __init__(self, data: str) -> None:
        for d in data.split(","):
            d = d.strip()
            count, color = d.split(" ")
            if color == "red":
                self.red = int(count)
            elif color == "green":
                self.green = int(count)
            elif color == "blue":
                self.blue = int(count)


@dataclass
class Game:
    def __init__(self, data: str):
        self.data = data
        self.id = int(data.split(":")[0].replace("Game ", ""))
        self.pulls: list[dict[str, int]] = [
            Pull(c.strip()) for c in data.split(":")[1].split(";")
        ]
        self.pull_count = len(self.pulls)
        self.max_red = max([p.red for p in self.pulls])
        self.max_green = max([p.green for p in self.pulls])
        self.max_blue = max([p.blue for p in self.pulls])


def problem_1(red: int, green: int, blue: int) -> int:
    possible_game_ids: list[int] = []
    with open("input.txt", "r") as input_file:
        for line in input_file:
            line = line.strip()
            game = Game(data=line)
            if (
                game.max_red <= red
                and game.max_green <= green
                and game.max_blue <= blue
            ):
                possible_game_ids.append(game.id)

    return sum(possible_game_ids)


def problem_2() -> int:
    game_min_cubes: list[int] = []
    with open("input.txt", "r") as input_file:
        for line in input_file:
            line = line.strip()
            game = Game(data=line)
            game_min_cubes.append(game.max_red * game.max_blue * game.max_green)

    return sum(game_min_cubes)


if __name__ == "__main__":
    print(f"Problem 1: {problem_1(red=12, green=13, blue=14)}")
    print(f"Problem 2: {problem_2()}")
