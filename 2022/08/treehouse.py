def part_one(input_data):
    return input_data


def part_two(input_data):
    return input_data


with open("input.txt", "r") as file:
    input_data = file.readlines()

print(part_one(input_data))
print(part_two(input_data))
