def part_one(input_data):
    return sum(
        [
            (a[0] >= b[0] and a[-1] <= b[-1]) or (b[0] >= a[0] and b[-1] <= a[-1])
            for a, b in input_data
        ]
    )


def part_two(input_data):
    return sum(
        [
            bool(set(range(a[0], a[1] + 1)) & set(range(b[0], b[1] + 1)))
            for a, b in input_data
        ]
    )


with open("input.txt", "r") as file:
    input_data = [line.strip().split(",") for line in file]
    input_pairs = [
        (list(map(int, x.split("-"))), list(map(int, y.split("-"))))
        for x, y in input_data
    ]

print(part_one(input_pairs))
print(part_two(input_pairs))
