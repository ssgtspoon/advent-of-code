package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	data := readFile()
	parsedData := parseFileData(data)
	fmt.Println(stepOne(parsedData))
	fmt.Println(stepTwo(parsedData))
}

func stepOne(data [][2][2]int) int {
	count := 0
	for _, x := range data {
		left := x[0]
		right := x[1]
		if (left[0] >= right[0] && left[1] <= right[1]) || (right[0] >= left[0] && right[1] <= left[1]) {
			count += 1
		}
	}
	return count
}

func between(a int, b [2]int) bool {
	return a >= b[0] && a <= b[1]
}

func stepTwo(data [][2][2]int) int {
	count := 0
	for _, x := range data {
		left := x[0]
		right := x[1]
		if between(right[0], left) || between(right[1], left) || between(left[0], right) || between(left[1], right) {
			count += 1
		}
	}
	return count
}

func readFile() []string {
	var data []string
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return data
}

func parseFileData(fileData []string) [][2][2]int {
	var data [][2][2]int
	r := regexp.MustCompile(`(?P<left_start>\d+)-(?P<left_end>\d+),(?P<right_start>\d+)-(?P<right_end>\d+)`)
	for _, x := range fileData {
		match := r.FindStringSubmatch(x)
		result := make(map[string]int)
		for i, name := range r.SubexpNames() {
			if i != 0 && name != "" {
				v, _ := strconv.Atoi(match[i])
				result[name] = v
			}
		}
		left := [2]int{result["left_start"], result["left_end"]}
		right := [2]int{result["right_start"], result["right_end"]}
		both := [2][2]int{left, right}
		data = append(data, both)
	}
	return data
}
