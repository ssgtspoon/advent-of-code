from __future__ import annotations

from dataclasses import dataclass, field
from typing import List, Dict


@dataclass
class File:
    name: str
    size: int


@dataclass
class Dir:
    name: str
    files_size: int = 0
    total_size: int = 0
    parent_dir: Dir = None  # type: ignore
    files: List[File] = field(default_factory=list)
    dirs: List[Dir] = field(default_factory=list)

    def get_cur_path(self):
        d = self
        path_list = [root.name]
        while d != root:
            path_list.append(d.name)
            d = d.parent_dir
        return "".join(reversed(path_list))


root = Dir(name="/")
all_dirs: Dict[str, int] = {}


def get_sum_under(i):
    total = 0
    for _, v in all_dirs.items():
        if v <= i:
            total += v
    return total


def get_smallest_thats_at_least(i):
    frontrunner = root.total_size
    for _, v in all_dirs.items():
        if v >= i and v < frontrunner:
            frontrunner = v
    return frontrunner


def set_dir_sizes(dir: Dir):
    current_size = dir.files_size
    for d in dir.dirs:
        current_size += set_dir_sizes(d)
    dir.total_size = current_size
    all_dirs[dir.get_cur_path()] = dir.total_size
    return current_size


def main(input_data: List[str]):
    current_dir = root
    for x in input_data:
        parts = x.split()
        if parts[0] == "$":
            if parts[1] == "cd":
                if parts[2] == "..":
                    current_dir = current_dir.parent_dir
                elif parts[2] == "/":
                    current_dir = root
                else:
                    for d in current_dir.dirs:
                        if d.name == parts[2] + "/":
                            current_dir = d
            elif parts[1] == "ls":
                continue  # Do nothing
        elif parts[0] == "dir":
            for d in current_dir.dirs:
                if d.name == parts[1] + "/":
                    break  # Already exists
            current_dir.dirs.append(Dir(name=parts[1] + "/", parent_dir=current_dir))
        else:
            current_dir.files_size += int(parts[0])
            current_dir.files.append(File(name=parts[1], size=int(parts[0])))
    set_dir_sizes(root)
    print(get_sum_under(100_000))
    total_space = 70_000_000
    space_needed = 30_000_000
    current_available = total_space - root.total_size
    need_to_free = space_needed - current_available
    print(get_smallest_thats_at_least(need_to_free))


with open("input.txt", "r") as file:
    input_data = file.readlines()

main(input_data)
