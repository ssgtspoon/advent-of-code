package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func main() {
	rawFileContents := readFile()
	fileData := parseFileData(rawFileContents)
	fmt.Println(partOne(fileData))
	fmt.Println(partTwo(fileData))
}

func partOne(data *[]int) int {
	var max int
	groups := sumGroups(data)
	for _, x := range groups {
		if x > max {
			max = x
		}
	}
	return max
}

func partTwo(data *[]int) int {
	var total int
	groups := sumGroups(data)
	sort.Ints(groups)
	reverseSlice(&groups)
	for _, x := range groups[0:3] {
		total += x
	}
	return total
}

func readFile() *[]string {
	var data []string
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return &data
}

func parseFileData(data *[]string) *[]int {
	var fileData []int
	for _, x := range *data {
		if x == "" {
			fileData = append(fileData, -1)
		} else {
			n, _ := strconv.Atoi(x)
			fileData = append(fileData, n)
		}
	}
	return &fileData
}

func reverseSlice(input *[]int) {
	inputLength := len(*input)
	output := make([]int, inputLength)
	for i, n := range *input {
		j := inputLength - i - 1
		output[j] = n
	}
	*input = output
}

func sumGroups(data *[]int) []int {
	var groups []int
	total := 0
	for _, x := range *data {
		if x == -1 {
			groups = append(groups, total)
			total = 0
			continue
		}
		total += x
	}
	groups = append(groups, total)
	return groups
}
