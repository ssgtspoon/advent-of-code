def part_one(all_cals):
    break_indexes = [i for i, x in enumerate(all_cals) if x is None] + [len(all_cals)]
    return max(
        sum(x)
        for x in [
            all_cals[b + 1 : break_indexes[i + 1]]
            for i, b in enumerate(break_indexes[:-1])
        ]
    )


def part_two(all_cals):
    break_indexes = [i for i, x in enumerate(all_cals) if x is None] + [len(all_cals)]
    chunks = [
        all_cals[b + 1 : break_indexes[i + 1]] for i, b in enumerate(break_indexes[:-1])
    ]
    sorted_sums = sorted([sum(x) for x in chunks], reverse=True)
    return sum(sorted_sums[0:3])


with open("input.txt", "r") as file:
    input_data = [int(line.strip()) if line.strip() else None for line in file]

print(part_one(input_data))
print(part_two(input_data))
