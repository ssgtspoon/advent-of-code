package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

var myThrowScores = map[string]int{
	"X": 1,
	"Y": 2,
	"Z": 3,
}
var versusScore = map[string]map[string]int{
	"X": {"A": 3, "B": 0, "C": 6},
	"Y": {"A": 6, "B": 3, "C": 0},
	"Z": {"A": 0, "B": 6, "C": 3},
}
var wldMap = map[string]map[string]string{
	"X": {"A": "Z", "B": "X", "C": "Y"},
	"Y": {"A": "X", "B": "Y", "C": "Z"},
	"Z": {"A": "Y", "B": "Z", "C": "X"},
}

func readFile() []string {
	var data []string
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return data
}

func parseFileData(fileData []string) [][]string {
	var data [][]string
	for _, x := range fileData {
		data = append(data, strings.Split(x, " "))
	}
	return data
}

func partOne(data [][]string) int {
	var totalScore int
	for _, x := range data {
		totalScore += myThrowScores[x[1]] + versusScore[x[1]][x[0]]
	}
	return totalScore
}

func partTwo(data [][]string) int {
	var totalScore int
	for _, x := range data {
		elfThrow := x[0]
		wld := x[1]
		myThrow := wldMap[wld][elfThrow]
		totalScore += myThrowScores[myThrow] + versusScore[myThrow][elfThrow]
	}
	return totalScore
}

func main() {
	fileData := readFile()
	parsedFileData := parseFileData(fileData)
	fmt.Println(partOne(parsedFileData))
	fmt.Println(partTwo(parsedFileData))
}
