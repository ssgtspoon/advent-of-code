def score(elf, me) -> int:
    return {"X": 1, "Y": 2, "Z": 3}[me] + {
        "X": {"A": 3, "B": 0, "C": 6},
        "Y": {"A": 6, "B": 3, "C": 0},
        "Z": {"A": 0, "B": 6, "C": 3},
    }[me][elf]


def part_one(throws):
    return sum([score(x, y) for x, y in throws])


def part_two(throws):
    return sum(
        [
            score(a, b)
            for a, b in [
                (
                    x,
                    {
                        "X": {"A": "Z", "B": "X", "C": "Y"},
                        "Y": {"A": "X", "B": "Y", "C": "Z"},
                        "Z": {"A": "Y", "B": "Z", "C": "X"},
                    }[y][x],
                )
                for x, y in throws
            ]
        ]
    )


with open("input.txt", "r") as file:
    throws = [(z.split()[0], z.split()[1]) for z in file]

print(part_one(throws))
print(part_two(throws))
