from string import ascii_letters


def part_one(input_data):
    pairs = [
        (line[0 : len(line.strip()) // 2], line.strip()[len(line.strip()) // 2 :])
        for line in input_data
    ]
    return sum([ascii_letters.find((set(x) & set(y)).pop()) + 1 for x, y in pairs])


def part_two(input_data):
    return sum(
        [
            ascii_letters.find(
                (
                    set(input_data[i : i + 3][0])
                    & set(input_data[i : i + 3][1])
                    & set(input_data[i : i + 3][2])
                ).pop()
            )
            + 1
            for i in range(0, len(input_data), 3)
        ]
    )


with open("input.txt", "r") as file:
    input_data = [line.strip() for line in file]

print(part_one(input_data))
print(part_two(input_data))
