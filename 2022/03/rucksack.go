package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

const ascii string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func intersect[T comparable](a []T, b []T) []T {
	set := make(map[T]struct{})
	hash := make(map[T]struct{})
	for _, v := range a {
		hash[v] = struct{}{}
	}

	for _, v := range b {
		if _, ok := hash[v]; ok {
			set[v] = struct{}{}
		}
	}
	keys := make([]T, 0, len(set))
	for k := range set {
		keys = append(keys, k)
	}
	return keys
}

func readFile() []string {
	var data []string
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return data
}

func stepOne(data []string) int {
	var total int
	for _, x := range data {
		a := strings.Split(x[:len(x)/2], "")
		b := strings.Split(x[len(x)/2:], "")
		s := intersect(a, b)
		for _, x := range s {
			total += strings.Index(ascii, x) + 1
		}
	}
	return total
}

func stepTwo(data []string) int {
	var total int
	var chunks [][]string
	for i := 0; i < len(data); i += 3 {
		end := i + 3
		if end > len(data) {
			end = len(data)
		}
		chunks = append(chunks, data[i:end])
	}
	for _, c := range chunks {
		c0 := strings.Split(c[0], "")
		c1 := strings.Split(c[1], "")
		c2 := strings.Split(c[2], "")

		s := intersect(intersect(c0, c1), c2)
		for _, x := range s {
			total += strings.Index(ascii, x) + 1
		}
	}
	return total
}

func main() {
	data := readFile()
	fmt.Println(stepOne(data))
	fmt.Println(stepTwo(data))
}
