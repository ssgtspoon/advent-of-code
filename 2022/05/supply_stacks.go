package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type instruction struct {
	count int
	from  int
	to    int
}

func main() {
	data := readFile()
	startingStack, instructionLines := parseInput(data)
	instructions := parseInstructions(instructionLines)
	stack := cleanAndPivotStack(startingStack)
	fmt.Println(stepOne(stack, instructions))
	fmt.Println(stepTwo(stack, instructions))
}

func stepOne(stack [][]string, instructions []instruction) string {
	var newStack [][]string
	for _, x := range stack {
		foo := make([]string, len(x))
		copy(foo, x)
		newStack = append(newStack, foo)
	}
	var tops []string
	for _, x := range instructions {
		count := x.count
		from := x.from - 1
		to := x.to - 1

		for i := 0; i < count; i++ {
			popped := newStack[from][len(newStack[from])-1]
			newStack[from] = newStack[from][:len(newStack[from])-1]
			newStack[to] = append(newStack[to], popped)
		}
	}
	for _, x := range newStack {
		tops = append(tops, x[len(x)-1])
	}
	return strings.Join(tops, "")
}

func stepTwo(stack [][]string, instructions []instruction) string {
	var newStack [][]string
	for _, x := range stack {
		foo := make([]string, len(x))
		copy(foo, x)
		newStack = append(newStack, foo)
	}
	var tops []string
	for _, x := range instructions {
		count := x.count
		from := x.from - 1
		to := x.to - 1

		popped := newStack[from][len(newStack[from])-count:]
		fmt.Println(count, popped)
		newStack[from] = newStack[from][:len(newStack[from])-count]
		newStack[to] = append(newStack[to], popped...)
	}
	for _, x := range newStack {
		tops = append(tops, x[len(x)-1])
	}
	return strings.Join(tops, "")
}

func readFile() []string {
	var data []string
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return data
}

func parseInput(data []string) ([]string, []string) {
	var stacks []string
	var instructions []string
	for i, line := range data {
		if line == "" {
			stacks = data[:i-1]
			instructions = data[i+1:]
			break
		}
	}
	return stacks, instructions
}

func cleanAndPivotStack(startingStack []string) [][]string {
	var cleaned [][]string
	var cleanedHoriz [][]string
	r := regexp.MustCompile(`.(.).\s.(.).\s.(.).\s.(.).\s.(.).\s.(.).\s.(.).\s.(.).\s.(.).`)
	for _, x := range startingStack {
		m := r.FindStringSubmatch(x)
		cleanedHoriz = append(cleanedHoriz, m[1:])
	}
	for x := 0; x < 9; x++ {
		cleaned = append(cleaned, []string{})
		for y := len(cleanedHoriz) - 1; y >= 0; y-- {
			if cleanedHoriz[y][x] != " " {
				cleaned[x] = append(cleaned[x], cleanedHoriz[y][x])
			}
		}
	}
	return cleaned
}

func parseInstructions(instructions []string) []instruction {
	r := regexp.MustCompile(`move (\d+) from (\d) to (\d)`)
	var data []instruction
	for _, x := range instructions {
		match := r.FindStringSubmatch(x)
		count, _ := strconv.Atoi(match[1])
		from, _ := strconv.Atoi(match[2])
		to, _ := strconv.Atoi(match[3])
		instr := instruction{
			count: count,
			from:  from,
			to:    to,
		}
		data = append(data, instr)
	}
	return data
}
