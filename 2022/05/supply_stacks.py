import re


def part_n(start_stacks, instructions, part):
    stacks = [x[:] for x in start_stacks]  # Manual "deep" copy
    for inst in instructions:
        pops = ([1] * inst["count"]) if part == 1 else list(range(inst["count"], 0, -1))
        for p in pops:
            stacks[inst["to"] - 1].append(stacks[inst["from"] - 1].pop(p * -1))
    return "".join([x[-1] for x in stacks])


with open("input.txt", "r") as file:
    instructions, h_stacks = [], []
    in_instructions = False
    instructions_re = re.compile(r"move (\d+) from (\d) to (\d)")
    for line in file:
        if not line.strip():
            in_instructions = True
            continue

        if in_instructions:
            m = instructions_re.match(line)
            if not m:
                continue
            instructions.append(
                {
                    "count": int(m.group(1)),
                    "from": int(m.group(2)),
                    "to": int(m.group(3)),
                }
            )
        else:
            h_stacks.append([x if x.strip() else None for x in list(line)[1::4]])

    start_stacks = [[y for y in x if y] for x in list(zip(*h_stacks[::-1]))]

print(part_n(start_stacks, instructions, 1))
print(part_n(start_stacks, instructions, 2))
