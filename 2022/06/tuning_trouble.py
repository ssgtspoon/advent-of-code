def part_one(input_data, packet_size):
    for i in range(len(input_data) - packet_size):
        if len(set(input_data[i : i + packet_size])) == packet_size:
            return i + packet_size
    return -1


with open("input.txt", "r") as file:
    input_data = file.read()

print(part_one(input_data, 4))
print(part_one(input_data, 14))
