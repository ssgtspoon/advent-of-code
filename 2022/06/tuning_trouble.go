package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	data := readFile()
	fmt.Println(stepOne(data, 4))
	fmt.Println(stepOne(data, 14))
}

func stepOne(data string, tokenLen int) int {
	for i := range data[:len(data)-tokenLen] {
		token := data[i : i+tokenLen]
		if distinctCharacters(token) == tokenLen {
			return i + tokenLen
		}
	}
	return -1
}

func distinctCharacters(data string) int {
	charMap := make(map[rune]struct{})
	for _, x := range data {
		charMap[x] = struct{}{}
	}
	return len(charMap)
}

func readFile() string {
	data, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	return string(data)
}
